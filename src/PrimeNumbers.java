import com.primenumber.fermat.FermatPrimalityTest;
import com.primenumber.millerrabin.MillerRabinPrimalityTest;

import java.math.BigInteger;
import java.util.concurrent.ThreadLocalRandom;

public class PrimeNumbers {
    /*
    This is implementation of primality test classes based on Fermat's and Miller-Rabin's theorems.
    This implementation performs check of both implementations on randomly generated longs and
    known prime numbers for sanity checks.
     */

    public static void main(String[] args) {
        // we will test algorithms on known prime numbers to try to check their sanity
        // have in mind that both algorithms are probabilistic therefore sanity test is just approximation
        int[] sanitySamples = new int[]{7789, 8069, 9011, 6397, 7129, 8693};

        // random numbers to test
        int[] testNumbers = new int[sanitySamples.length];
        for (int i=0; i<testNumbers.length; i++) {
            // dirty hack to generate longs from natural range
            // as it's for EDU purposes, limit the range
            testNumbers[i] = ThreadLocalRandom.current().nextInt(1, 99999);
        }

        // we have to specify accuracy of test
        // accuracy means number of attempts to check primality
        int accuracy = 150;

        // Fermat primality of number check
        FermatPrimalityTest fermatPrime = new FermatPrimalityTest();

        // Miller - Rabin primality of number check
        MillerRabinPrimalityTest millerRabinPrime = new MillerRabinPrimalityTest();

        // run tests
        for (int i=0; i<testNumbers.length; i++) {
            int[] numbers = new int[]{testNumbers[i], sanitySamples[i]};
            // run for random number and sanity check
            for(int number : numbers) {
                if (fermatPrime.isPrimary(number, accuracy)) {
                    System.out.printf("After Fermat check with accuracy %d we know that number %d is probably primary.%n", accuracy, number);
                } else {
                    System.out.printf("After Fermat check with accuracy %d we know that number %d is composite.%n", accuracy, number);
                }
                if (millerRabinPrime.isPrimary(number, accuracy)) {
                    System.out.printf("After Miller-Rabin check with accuracy %d we know that number %d is probably primary.%n", accuracy, number);
                } else {
                    System.out.printf("After Miller-Rabin check with accuracy %d we know that number %d is composite.%n", accuracy, number);
                }
            }
        }

    }

}
