package com.primenumber.fermat;

import java.math.BigInteger;
import java.util.concurrent.ThreadLocalRandom;

public class FermatPrimalityTest {
    /*
    This class implements primality check using Fermat theorem
    class operates on standard integer data types (long and int)

    This function uses java.math BigInt type, as this object has lot of built-in
    functions allowing operations on very big numbers eg. long type powered by long type.
    ref. https://crypto.stanford.edu/pbc/notes/numbertheory/millerrabin.html
     */

    public boolean isPrimary(int number, int accuracy) {
        // checked number has to be natural
        if (number < 1)
            throw new IllegalArgumentException("Checked number has to be natural");

        // if given number is 1 return false
        // 1 is natural but is not odd
        if (number == 1)
            return false;

        // for optimization 2 and 3 are well known prime numbers
        if (number == 2 || number == 3)
            return true;

        // quick return false if number is even
        // even number can't be primary
        // https://en.wikipedia.org/wiki/Prime_number
        if (number % 2 == 0)
            return false;

        boolean primary = false;

        // run number of checks equal to accuracy value
        for (int i=0; i < accuracy; i++) {
            // in Fermat theorem we have condition
            // a -> 1 < a < p
            // max. range may be equal to number as for nextInt() upper range is exclusive
            int a = ThreadLocalRandom.current().nextInt(2, number);

            // our a has to be relatively first with number
            // GCD(p,a) == 1
            while (gcd(number, a) != 1) {
                a = ThreadLocalRandom.current().nextInt(2, number);
            }

            // cast to BigInteger for powering
            BigInteger bigA = BigInteger.valueOf(a);

            // in Fermat theorem we have to perform calculation
            // .pow(a, n-1) % n

            // first power a to n-1
            BigInteger poweredNumber = bigA.pow(number-1);

            // then find modulo
            // cast to String to iterate over digits
            String powered = poweredNumber.toString();

            // use Modular multiplication theorem
            // ref. https://www.khanacademy.org/computing/computer-science/cryptography/modarithmetic/a/modular-multiplication
            int modulo = 0;
            String moduloCharNumber = "";

            for (int n=0; n < powered.length(); n++) {
                moduloCharNumber = moduloCharNumber + powered.charAt(n);
                modulo = Integer.parseInt(moduloCharNumber) % number;
                moduloCharNumber = "" + modulo;
            }

            if(Integer.parseInt(moduloCharNumber) != 1) {
                primary = false;
                break;
            } else {
                primary = true;
            }
        }

        return primary;
    }

    private static int gcd(int first, int second)
    {
        if (second == 0)
        {
            return first;
        }
        else
        {
            return gcd(second, first%second);
        }
    }
}
