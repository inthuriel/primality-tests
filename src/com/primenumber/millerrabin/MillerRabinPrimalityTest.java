package com.primenumber.millerrabin;

import java.math.BigInteger;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

public class MillerRabinPrimalityTest {
    /*
    This class implements primality check using Miller - Rabin theorem
    class operates on standard integer data types (long and int)

    This function uses java.math BigInt type, as this object has lot of built-in
    functions allowing operations on very big numbers eg. long type powered by long type.
    ref. https://crypto.stanford.edu/pbc/notes/numbertheory/millerrabin.html
     */

    public boolean isPrimary(int number, int accuracy) {
        // checked number has to be natural
        if (number < 3)
            throw new IllegalArgumentException("Checked number has to be natural and > 3");

        // quick return false if number is even
        // even number can't be primary
        // https://en.wikipedia.org/wiki/Prime_number
        if (number % 2 == 0)
            return false;

        boolean primary = true;

        // calculate number - 1 according to theorem to calculate 2 power dividing number
        int decreasedNumber = number - 1;

        // start values for power 2 searching loop
        int maxTwoPow = 1;
        int d = 0;

        // search for maximal power of 2 dividing number - 1
        for (; d % 2 == 0; maxTwoPow++) {
            d = (int) (decreasedNumber / Math.pow(2, maxTwoPow));
        }

        // run number of checks equal to accuracy value
        // label loop for proper jump out when nested
        ProofLoop: for (int i=0; i < accuracy; i++) {
            // get a where a is equal to
            // 2 <= a < n
            int a = ThreadLocalRandom.current().nextInt(2, number - 2);

            // find X equals to
            // a powered d modulo number

            // first power a to d
            BigInteger poweredNumber = BigInteger.valueOf((long) Math.pow(a, d));

            // then find modulo
            // cast to String to iterate over digits
            String powered = poweredNumber.toString();

            // use Modular multiplication theorem
            // ref. https://www.khanacademy.org/computing/computer-science/cryptography/modarithmetic/a/modular-multiplication
            int modulo = 0;
            String x = "";

            for (int n=0; n < powered.length(); n++) {
                x = x + powered.charAt(n);
                modulo = Integer.parseInt(x) % number;
                x = "" + modulo;
            }

            if (Integer.parseInt(x) == 0 || Integer.parseInt(x) == number - 1) {
                continue;
            }

            int r = 0;
            for (; r < maxTwoPow; r++) {

                // first power a to 2
                BigInteger aPoweredNumber = BigInteger.valueOf(Integer.parseInt(x)).pow(2);

                // then find modulo
                // cast to String to iterate over digits
                String aPowered = aPoweredNumber.toString();

                // use Modular multiplication theorem
                // ref. https://www.khanacademy.org/computing/computer-science/cryptography/modarithmetic/a/modular-multiplication
                int modulo1 = 0;
                String moduloCharNumber = "";

                for (int n = 0; n < aPowered.length(); n++) {
                    moduloCharNumber = moduloCharNumber + aPowered.charAt(n);
                    modulo1 = Integer.parseInt(moduloCharNumber) % number;
                    moduloCharNumber = "" + modulo1;
                }

                if (Integer.parseInt(moduloCharNumber) == number - 1) {
                    continue ProofLoop;
                }

                if (Integer.parseInt(moduloCharNumber) == 1) {
                    primary = false;
                    break ProofLoop;
                }
            }
        }

        return primary;
    }

    /*
    Method to generate random BigInteger number within range
    range inclusive
     */
    private static BigInteger randomBigInteger(BigInteger rangeStart, BigInteger rangeEnd) {
        Random random = new Random();
        int maxNumBitLength = rangeEnd.bitLength();
        BigInteger randomBigInt;
        do {
            randomBigInt = new BigInteger(maxNumBitLength, random);
        } while (randomBigInt.compareTo(rangeStart) < 0 || randomBigInt.compareTo(rangeEnd) > 0);
        return randomBigInt;
    }
}
