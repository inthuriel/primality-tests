# Primality of number check

This repository contains *Java* classes implementing primality checks based on:

- Fermat
- Miller - Rabin  

theorems.
  
This implementation performs check of both implementations on randomly generated longs and known prime numbers for sanity checks.  

This product comes without the warranty :)
